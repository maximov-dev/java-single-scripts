import java.io.Console;
import java.io.*;
import java.util.*;

public class HelloWorld {
    public static void main(String[] args) {
        Double[] array = new Double[100];

        String[] array_split = args[0].split("=");
        array_split =  array_split[1].split(",");


        Double result = 0.0;


        for(int i = 0; i < array_split.length ; i++) {
            try {
                array[i] = Double.parseDouble(array_split[i]);

            } catch (IOError | NumberFormatException e) {
                continue;

            }

            result += Poly(array[i]);
        }

        System.out.println("Результат: " + result);
    }

    private static Double Poly(Double value) {
        return 1 / (value * 3);
    }
}

